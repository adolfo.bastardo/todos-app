describe('The Home Page', () => {
  it('successfully loads', () => {
    cy.visit('/');
    // cy.get('button').click({ position: 'topRight', force: true }); // This would throws errors in cognito
    cy.get('h3').contains('Bienvenidos a mi "Todos App", por favor iniciar sesión para continuar.');
  });
});