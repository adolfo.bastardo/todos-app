import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

test('renders expect Bienvenidos text', () => {
  render(<App />);
  const welcomeElement = screen.getByText(/Bienvenidos/i);
  expect(welcomeElement).toBeInTheDocument();
});
