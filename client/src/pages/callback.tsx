import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { withAuth, WithAuthProps } from "@8base-react/auth";
import { createHttpLink, useApolloClient } from "@apollo/client";
import { setContext } from '@apollo/client/link/context';

import * as gql from '../shared/graphql';

const httpLink = createHttpLink({
    uri: process.env.REACT_APP_WORKSPACE_ENDPOINT ?? '/',
});

const authLink = (idToken: string) => setContext((_, { headers }) => {
  // return the new auth headers
  return {
    headers: {
      ...headers,
      authorization: idToken.length > 0 ? `Bearer ${idToken}` : "",
    }
  }
});

/**
 *  This page receives the response from AWS Cognito and
 *  set the auth status
 * 
 * @param props 
 */
const CallbackPage: React.FC<WithAuthProps> = (props) => {
    const { auth } = props;
    const history = useHistory();
    const clientApollo = useApolloClient();

    const [idTokenSaved, setIdTokenSaved] = useState('');

    useEffect(() => {
        let cancel = false;

        const handleAuthentication = async ({ idToken, email }: {
            idToken: string;
            email: string;
        }) => {
            /**
             * Check if user exists in 8base.
             */
            try {
                await clientApollo.query({
                    query: gql.CURRENT_USER_QUERY,
                    context: {
                        headers: {
                            authorization: `Bearer ${idToken}`,
                        }
                    }
                });
            } catch {
                /**
                 * If user doesn't exist, an error will be
                 * thrown, which then the new user can be
                 * created using the authResult values.
                 */
                await clientApollo.mutate({
                    mutation: gql.USER_SIGN_UP_MUTATION,
                    variables: {
                        user: { email: email },
                        authProfileId: process.env.REACT_APP_AUTH_PROFILE_ID,
                    }
                });
            }
        };
        const callbackDidMount = async () => {
            if (!cancel) {
                /* Get authResult from auth client after redirect */
                const authResult = await auth.authClient.getAuthorizedData();
    
                if (authResult.idToken.length > 0) {
                    /* Identify or create user record using authenticated details */
                    await handleAuthentication(authResult);
        
                    /* Add the idToken to the auth state */
                    auth.authClient.setState({ token: authResult.idToken });
                    setIdTokenSaved(authResult.idToken);
                }
            }
        };
        callbackDidMount();

        return () => {
            cancel = true;
        };
    }, [auth, clientApollo, setIdTokenSaved]);

    useEffect(() => {
        if (idTokenSaved.length > 0) {
            /**
             * Auth headers for communicating with the 8base API.
             */
            clientApollo.setLink(authLink(idTokenSaved).concat(httpLink));
            history.push('/tasks');
        }
    }, [idTokenSaved, clientApollo, history]);

    return (<div>Loading user...</div>);
};

const CallbackPageWithAuth = withAuth(CallbackPage);

export default CallbackPageWithAuth;
