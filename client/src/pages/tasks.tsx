import React, { useEffect, useState } from "react";
import { useLazyQuery, useMutation } from "@apollo/client";
import { withAuth, WithAuthProps } from "8base-react-sdk";
import { useFormik } from "formik";
import {
  Button,
  Divider,
  Grid,
  makeStyles,
  TextField,
  Typography,
  Snackbar,
  IconButton
} from "@material-ui/core";
import {
  Pagination
} from "@material-ui/lab";
import {
  CloseRounded
} from '@material-ui/icons';


import TaskComponent from "../shared/components/task";
import * as gql from '../shared/graphql';
import { Task, TaskCreatedResponse, TasksListResponse } from "../shared/responses_types";
 
interface TaskCreateForm {
  name: string;
  description: string;
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding: 10,
    paddingTop: 50,
  },
  divider: {
    padding: '10px 0'
  },
  formContainer: {
    textAlign: 'right',
    '& > form > *': {
      marginLeft: 10,
      marginRight: 10,
    },
  },
  button: {
    padding: '14px 11px'
  },
  pagination: {
    justifyContent: 'center',
    padding: '20px 0',
    '& > li': {
      textAlign: 'center',
    }
  },
  taskListHeader: {
    margin: '0 10px'
  }
}));

/**
 *  Task Page for the App
 *  Here is defined the primary structure for show the tasks for an user
 * 
 * @param props 
 */
const TasksPage: React.FC<WithAuthProps> = (props) => {
  const classes = useStyles();
  const { auth: { isAuthorized } } = props;
  const [getTasks, {loading, error, data, fetchMore}] = useLazyQuery<TasksListResponse>(gql.TasksList);
  const [createTasks] = useMutation<TaskCreatedResponse>(gql.TaskCreate);

  const [openSnack, setOpenSnack] = React.useState(false);
  const [messageSnack, setMessageSnack] = React.useState('');

  // total pages
  const [pages, setPages] = useState<number>(0);
  // current page
  const [paginationPage, setPaginationPage] = useState<number>(1);

  // Id of the first task in this page - only set if the current page is greater than 1
  // Is used for pagination
  const [beforeId, setBeforeId] = useState<string | null>(null);

  // Id of the last task - only set if there are more pages
  // Is used for pagination
  const [afterId, setAfterId] = useState<string | null>(null);

  // Tasks for this page
  const [tasks, setTasks] = useState<Task[]>([]);

  useEffect(() => {
    // if logged we will get the first page
    if(isAuthorized) {
      getTasks({
        variables: {
          first: 10,
          orderBy: gql.TaskOrderBy.id_DESC
        }
      });
    }
  }, [isAuthorized, getTasks]);

  useEffect(() => {
    if (data !== undefined) {
      const taskData = data.tasksList;

      // Get the total pages
      const pages = Math.ceil(Math.abs(taskData.count / 10));
      setPages(pages);

      const items = taskData.items;
      setTasks(items);
      
      // Set the last id for next pagination
      if (pages > 1 && pages > paginationPage) {
        setAfterId(items[items.length - 1].id);
      } else {
        setAfterId(null);
      }

      // Set the first id for previous pagination
      if (paginationPage > 1) {
        setBeforeId(items[0].id);
      } else {
        setBeforeId(null);
      }
    }
  }, [data, paginationPage]);

  // Show utility for useful messages
  const showSnack = (message: string) => {
    setMessageSnack(message);
    setOpenSnack(true);
  };

  const handleCloseSnack = (event: React.SyntheticEvent | React.MouseEvent, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnack(false);
    setMessageSnack('');
  };

  const handlePagination = (event: React.ChangeEvent<unknown>, value: number) => {
    if (fetchMore) {
      let variables: {
        first: number;
        orderBy: gql.TaskOrderBy;
        after?: string;
        before?: string;
      } = {
        first: 10,
        orderBy: gql.TaskOrderBy.id_DESC
      };

      // Define if is next, previous or first page
      if (paginationPage < value && afterId) {
        variables.after = afterId;
      } else if (paginationPage > value && beforeId) {
        variables.before = beforeId;
      }

      fetchMore({
        variables,
      }).then(() => {
        // Set page only if we have obtained the data
        setPaginationPage(value);
      });
    }
  };

  const initialValues: TaskCreateForm = { name: '', description: ''};
  const formik = useFormik({
    initialValues,
    validate: (values) => {
      const errors: { name?: string; } = {};
      if (!values.name) {
        errors.name = 'Requerido';
      }
      return errors;
    },
    onSubmit: (values, { setSubmitting }) => {
      console.log('Creando Tarea: ', JSON.stringify(values, null, 2));
      createTasks({
        variables: values
      }).then(created => {
        showSnack('¡Se ha creado la tarea!');
        if (fetchMore) {
          fetchMore({
            variables: {
              first: 10,
              orderBy: gql.TaskOrderBy.id_DESC
            }
          }).then(() => {
            // Set first page to get the new result and reset tasks in view
            setPaginationPage(1);
          });
        }
        setSubmitting(false);
      }).catch((e) => {
        showSnack('Hubo un error al intentar crear la tarea');
        console.log("Error al crear la tarea", e.message);
        setSubmitting(false);
      })
    }
  });

  // Utility for update Task in sub-component
  const handleTaskChange = () => {
    if (fetchMore) {
      fetchMore({
        variables: {
          first: 10,
          orderBy: gql.TaskOrderBy.id_DESC
        }
      }).then(() => {
        // Set first page to get the new result and reset tasks in view
        setPaginationPage(1);
      });
    }
  }

  return <>
    <Grid container className={classes.root} justify="center">
      <Grid item xs={12}>
        <Grid container justify="space-between" alignItems="center">
          <Grid item xs={4}>
            <Typography variant="h3">Tareas</Typography>
          </Grid>
          <Grid item xs={8} className={classes.formContainer}>
            <form onSubmit={formik.handleSubmit}>
              <TextField
                id="outlined-name"
                label="Nombre"
                name="name"
                variant="outlined"
                value={formik.values.name}
                onChange={formik.handleChange}
                error={formik.touched.name && Boolean(formik.errors.name)}
                helperText={formik.touched.name && formik.errors.name }
              />
              <TextField
                id="outlined-desc"
                label="Descripción"
                name="description"
                variant="outlined"
                value={formik.values.description}
                onChange={formik.handleChange}
                error={formik.touched.description && Boolean(formik.errors.description)}
                helperText={formik.touched.description && formik.errors.description }
              />
              <Button color="secondary" size="large" className={classes.button} type="submit">
                Agregar Tarea
              </Button>
            </form>
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={12} className={classes.divider}>
          <Divider variant="middle" />
      </Grid>
      {
        loading ?
          <Grid item xs={12}>
            <Typography variant="h4">Cargando...</Typography>
          </Grid>
        : error ?
          <Grid item xs={12}>
            <Typography variant="h4">Error al cargar los datos...</Typography>
          </Grid>
          :
          <Grid item xs={12} >
            <Grid container justify="space-between" alignItems="center" className={classes.taskListHeader}>
              <Grid item xs={1}>
                <Typography variant="h6">Listo</Typography>
              </Grid>
              <Grid item xs={2}>
                <Typography variant="h6">Nombre</Typography>
              </Grid>
              <Grid item xs={5}>
                <Typography variant="h6">Description</Typography>
              </Grid>
              <Grid item xs={3}>
                <Typography variant="h6">Creado el</Typography>
              </Grid>
              <Grid item xs={1}>
                <Typography variant="h6">Opciones</Typography>
              </Grid>
            </Grid>
            {
              tasks && tasks.length > 0  ?
              // There are tasks
              tasks.map((task: Task, i: number) => <React.Fragment key={`task-grid-${i}`}>
                <TaskComponent task={task} parentMesageSnackHandle={showSnack} onChange={handleTaskChange} />
              </React.Fragment>) :
              // There aren't tasks
              <Grid container justify="space-between" alignItems="center">
                <Grid item xs={12} style={{
                  textAlign: "center"
                }}>
                  <Typography variant="body2">No hay tareas registradas</Typography>
                </Grid>
              </Grid>
            }
          </Grid>
      }
      <Grid item xs={4} className={classes.pagination}>
        <Pagination
          classes={{
            ul: classes.pagination
          }}
          color="primary"
          count={pages}
          size="medium"
          className={classes.pagination}
          page={paginationPage}
          onChange={handlePagination}
        />
      </Grid>
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        open={openSnack}
        autoHideDuration={3000}
        onClose={handleCloseSnack}
        message={messageSnack}
        action={
          <IconButton size="small" aria-label="close" color="inherit" onClick={handleCloseSnack}>
            <CloseRounded fontSize="small" />
          </IconButton>
        }
      />
    </Grid>
  </>;
};

const TasksPageWithAuth = withAuth(TasksPage);

export default TasksPageWithAuth;