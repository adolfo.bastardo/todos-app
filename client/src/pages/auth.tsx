import React, { useEffect } from "react";
import { withAuth, WithAuthProps } from "@8base-react/auth";

/**
 * This page triggers the auth process
 * 
 * @param props 
 */
const AuthPage: React.FC<WithAuthProps> = (props) => {
    const { auth } = props;

    useEffect(() => {
        const authorize = async () => {
            if(!auth.isAuthorized) {
                await auth.authClient.authorize();
            }
        }
        authorize();
    }, [auth]);

    return (<div>Wait while is authorized...</div>);
};

const AuthPageWithAuth = withAuth(AuthPage);

export default AuthPageWithAuth;
