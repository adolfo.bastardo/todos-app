import React, { useEffect } from "react";
import { useHistory } from "react-router-dom";
import { withAuth, WithAuthProps } from "8base-react-sdk";
import { Grid, Typography } from "@material-ui/core";

/**
 *  Home Page for the App
 * 
 * @param props 
 */
const HomePage: React.FC<WithAuthProps> = (props) => {
  const { auth: { isAuthorized } } = props;
  const history = useHistory();

  useEffect(() => {
    // if is already logged redirect to the tasks page
    if (isAuthorized) {
      history.push("/tasks");
    }
  }, [isAuthorized, history]);

  return (<Grid container justify="center">
    <Grid item xs={6} style={{
      textAlign: "center"
    }}>
      <Typography variant="h3">
        Bienvenidos a mi "Todos App", por favor iniciar sesión para continuar.
      </Typography>
    </Grid>
  </Grid>);
};

const HomePageWithAuth = withAuth(HomePage);

export default HomePageWithAuth;
