import { gql } from '@apollo/client';
/**
 * Query the current users details.
 */
export const CURRENT_USER_QUERY = gql`
  query CurrentUser {
    user {
      id
      email
      lastName
      firstName
    }
  }
`;


/**
 * Sign up a new user mutation.
 */
export const USER_SIGN_UP_MUTATION = gql`
  mutation UserSignUp($user: UserCreateInput!, $authProfileId: ID) {
    userSignUpWithToken(user: $user, authProfileId: $authProfileId) {
      id
      email
    }
  }
`;

/**
 * Tasks Types
 */

export enum TaskOrderBy {
  id_ASC = 'id_ASC',
  id_DESC = 'id_DESC',
  name_ASC = 'name_ASC',
  name_DESC = 'name_DESC',
  description_ASC = 'description_ASC',
  description_DESC = 'description_DESC',
  isDone_ASC = 'isDone_ASC',
  isDone_DESC = 'isDone_DESC',
  createdAt_ASC = 'createdAt_ASC',
  createdAt_DESC = 'createdAt_DESC',
  updatedAt_ASC = 'updatedAt_ASC',
  updatedAt_DESC = 'updatedAt_DESC',
  deletedAt_ASC = 'deletedAt_ASC',
  deletedAt_DESC = 'deletedAt_DESC',
}

/**
 * Tasks Queries
 */
export const TasksList = gql`
  query TasksList(
    $after: String,
    $before: String,
    $first: Int = 10,
    $last: Int,
    $orderBy: [TaskOrderBy] = id_DESC,
    $withDeleted: Boolean = false
  ) {
    tasksList(
      after: $after,
      before: $before,
      first: $first,
      last: $last,
      orderBy: $orderBy,
      withDeleted: $withDeleted
    ) {
      count
      items {
        id
        name
        description
        isDone
        createdAt
        deletedAt
        updatedAt
      }
    }
  }
`;

/**
 * Tasks Mutations
 */
export const TaskCreate = gql`
  mutation TaskCreate(
    $name: String!,
    $description: String,
  ) {
    taskCreate(
      data: {
        name: $name,
        description: $description
      }
    ) {
      id
      name
      description
      isDone
      createdAt
      deletedAt
      updatedAt
    }
  }
`;

export const TaskUpdate = gql`
  mutation TaskUpdate(
    $id: ID!
    $name: String!,
    $description: String,
  ) {
    taskUpdate(
      data: {
        id: $id,
        name: $name,
        description: $description
      },
      filter: {
        id: $id
      }
    ) {
      id
      name
      description
      isDone
      createdAt
      deletedAt
      updatedAt
    }
  }
`;


export const TaskDelete = gql`
  mutation TaskDelete(
    $id: ID!
  ) {
    taskDelete(
      data: {
        id: $id
      },
      filter: {
        id: $id
      }
    ) {
      success
    }
  }
`;

export const TaskSetDone = gql`
  mutation TaskSetDone(
    $id: ID!,
    $done: Boolean!
  ) {
    setTodoDone(
      id: $id,
      done: $done
    ) {
      success
    }
  }
`;
