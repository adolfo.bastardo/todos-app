export type CurrentUser = {
  user: {
    id: string;
    email: string;
    lastName: string;
    firstName: string;
  };
};

export type Task = {
  id: string;
  name: string;
  description: string;
  isDone: boolean;
  createdAt: string;
  updatedAt: string;
  deletedAt: string;
};

export type TasksListResponse = {
  tasksList: {
    count: number;
    items: Task[];
  }
};

export type TaskCreatedResponse = {
  taskCreate: Task;
};

export type TaskUpdateResponse = {
  taskUpdate: Task;
};

export type TaskDeleteResponse = {
  taskDelete: {
    success: boolean;
  };
};

export type TaskSetDoneResponse = {
  setTodoDone: {
    success: boolean;
  };
};
