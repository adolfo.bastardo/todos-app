import { Auth, AUTH_STRATEGIES, ISubscribableAuthClient } from '@8base/auth';

const domain = process.env.REACT_APP_AUTH_DOMAIN;
const clientId = process.env.REACT_APP_AUTH_CLIENT_ID;

const logoutRedirectUri = `${window.location.origin}/`;
const redirectUri = `${window.location.origin}/auth/callback`;

const client: ISubscribableAuthClient = Auth.createClient(
    {
      strategy: AUTH_STRATEGIES.WEB_COGNITO,
      subscribable: true,
    },
    {
      clientId,
      domain,
      redirectUri,
      logoutRedirectUri,
    }
) as ISubscribableAuthClient;

export default client;
