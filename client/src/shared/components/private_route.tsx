import React from "react";
import { withAuth, WithAuthProps } from '@8base-react/auth';
import { Route, Redirect, useHistory } from 'react-router-dom';

/**
 * Private Component what wrappes secure routes to check login status
 */
const PrivateRouteComponent: React.FC<WithAuthProps & {
    Component: React.FC | React.ComponentType;
}> = (props) => {
    const {
        auth: { isAuthorized },
        Component,
        ...rProps
    } = props;

    const history = useHistory();

    const renderRoute = (): React.ReactNode => {
        /**
         * Render component if user is logged in
         */
        if (isAuthorized) {
            return <Component />
        }

        /**
         * Redirect to login page if it's not authenticated
         */
        return <Redirect to={{ pathname: '/', state: { from: history.location } }} />;
    };

    return <Route {...rProps} render={renderRoute} />;
};

const PrivateRouteWithAuth = withAuth(PrivateRouteComponent)

export default PrivateRouteWithAuth;
