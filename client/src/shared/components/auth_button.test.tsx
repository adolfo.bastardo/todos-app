import React from 'react';
import { render, screen } from '@testing-library/react';
import { MockedProvider, MockedResponse } from '@apollo/client/testing';
import { SubscribableDecorator } from '@8base/auth';
import { AuthProvider } from '8base-react-sdk';

import AuthButtonComponent from './auth_button';
import { DummyAuthClient } from '../../util_test';
import { CURRENT_USER_QUERY } from '../graphql';

const mocks: MockedResponse<Record<string, any>>[] = [];

test('Auth Button without login', () => {
  const authClient = DummyAuthClient();
  const subscribableAuthClient = SubscribableDecorator.decorate(authClient);
  render(
    <AuthProvider authClient={subscribableAuthClient}>
      <MockedProvider mocks={mocks} addTypename={false}>
        <AuthButtonComponent />
      </MockedProvider>
    </AuthProvider>
  );

  expect(screen.getByText(/Login/i)).toBeTruthy();
});

// Mock data to test component in logged state
const mocksLogged: MockedResponse<Record<string, any>>[] = [
  {
    request: {
      query: CURRENT_USER_QUERY,
    },
    result: {
      data: {
        user: {
          id: "ciai",
          email: "abastardo@liberarte.org.ve",
          lastName: "Bastardo",
          firstName: "Adolfo"
        },
      },
    },
  },
];

test('Auth Button with login', async () => {
  const authClient = DummyAuthClient();
  const subscribableAuthClient = SubscribableDecorator.decorate(authClient);

  // Set a fake token to login
  subscribableAuthClient.setState({
    token: 'some-token'
  });

  render(
    <AuthProvider authClient={subscribableAuthClient}>
      <MockedProvider mocks={mocksLogged} addTypename={false}>
        <AuthButtonComponent />
      </MockedProvider>
    </AuthProvider>
  );

  // We wait for mock provider to resolve our fake query
  await new Promise(resolve => setTimeout(resolve, 0));
  expect(screen.getByText(/Logout/i)).toBeTruthy();
});
