import React from "react";
import { useMutation } from "@apollo/client";
import { useFormik } from "formik";
import {
  Paper,
  makeStyles,
  Typography,
  Checkbox,
  IconButton,
  TextField,
  Grid,
  Tooltip
} from "@material-ui/core";
import {
  EditRounded,
  DeleteRounded,
  SaveRounded,
  CancelRounded
} from "@material-ui/icons";

import * as gql from '../graphql';
import { Task, TaskDeleteResponse, TaskSetDoneResponse, TaskUpdateResponse } from "../responses_types";

// Form Interface to update data in task
interface TaskUpdateForm {
  id: string;
  name: string;
  description: string;
}

const useStyles = makeStyles((theme) => ({
  root: {
    margin: 10,
    padding: '10px 0',
  },
  formContainer: {
    '& > *': {
      marginLeft: 10,
      marginRight: 10,
    },
  },
  }));

/**
 *  Task Component
 *  Here is defined the layout for each task of the user
 *  and the actions related to this object
 * 
 * @param props 
 */
const TaskComponent: React.FC<{
  task: Task,
  parentMesageSnackHandle: (message: string) => void,
  onChange: () => void
}> = (props) => {
  const classes = useStyles();

  // Task fields
  const { id, name, description, isDone, createdAt } = props.task;

  const [deleteTask] = useMutation<TaskDeleteResponse>(gql.TaskDelete);
  const [updateTask] = useMutation<TaskUpdateResponse>(gql.TaskUpdate);
  const [setDoneTask] = useMutation<TaskSetDoneResponse>(gql.TaskSetDone);

  const [edit, setEdit] = React.useState(false);

  const handleSetDoneStatus = (done: boolean) => {
    setDoneTask({
      variables: {
        id,
        done: !done
      }
    }).then((res) => {
      props.parentMesageSnackHandle(!done ? '¡Se ha completado la tarea!' : 'Se ha marcado la tarea como "En curso"');

      // Notify parent component for this change
      props.onChange();
    }).catch((e) => {
      props.parentMesageSnackHandle('Hubo un error al intentar completar la tarea');
      console.log("Error al completar la tarea", e.message);
    });
  };

  const handleDeleteTask = () => {
    deleteTask({
      variables: {
        id
      }
    }).then((res) => {
      if (res.errors === undefined) {
        props.parentMesageSnackHandle('¡Se ha borrado la tarea!');
        props.onChange();
      }
    }).catch((e) => {
      props.parentMesageSnackHandle('Hubo un error al intentar borrar la tarea');
      console.log("Error al borrar la tarea", e.message);
    });
  };

  const initialValues: TaskUpdateForm = { id, name, description };
  const formik = useFormik({
    initialValues,
    validate: (values) => {
      const errors: { name?: string; } = {};
      if (!values.name) {
        errors.name = 'Required';
      }
      return errors;
    },
    onSubmit: (values, { setSubmitting }) => {
      console.log('Actualizando Tarea: ', JSON.stringify(values, null, 2));
      updateTask({
        variables: values
      }).then(created => {
        props.parentMesageSnackHandle('¡Se ha actualizado la tarea!');
        props.onChange();
        setEdit(false);
        setSubmitting(false);
      }).catch((e) => {
        props.parentMesageSnackHandle('Hubo un error al intentar actualizar la tarea');
        console.log("Error al crear la tarea", e.message);
        setSubmitting(false);
      })
    }
  });

  return <Paper elevation={3} className={classes.root}>
    {
      !edit ? <>
        <Grid container justify="space-between" alignItems="center">
            <Grid item xs={1}>
              <Checkbox
                checked={isDone}
                onChange={() => handleSetDoneStatus(isDone)}
                inputProps={{ 'aria-label': 'is done?' }}
              />
            </Grid>
            <Grid item xs={2}>
              <Typography variant="h6">{name}</Typography>
            </Grid>
            <Grid item xs={5}>
              <Typography component="span">{description}</Typography>
            </Grid>
            <Grid item xs={3}>
              <Typography component="span">{createdAt}</Typography>
            </Grid>
            <Grid item xs={1}>
              <div>
                <Tooltip title="Editar">
                  <IconButton onClick={() => setEdit(true)}>
                    <EditRounded />
                  </IconButton>
                </Tooltip>
                <Tooltip title="Eliminar">
                  <IconButton onClick={() => handleDeleteTask()}>
                    <DeleteRounded />
                  </IconButton>
                </Tooltip>
              </div>
            </Grid>
        </Grid>
      </> : <>
        <form onSubmit={formik.handleSubmit}>
          <Grid container justify="space-between" alignItems="center">
            <Grid item xs={11} className={classes.formContainer}>
                <TextField
                  label="Nombre"
                  name="name"
                  variant="outlined"
                  value={formik.values.name}
                  onChange={formik.handleChange}
                  error={formik.touched.name && Boolean(formik.errors.name)}
                  helperText={formik.touched.name && formik.errors.name }
                />
                <TextField
                  label="Descripción"
                  name="description"
                  variant="outlined"
                  value={formik.values.description}
                  onChange={formik.handleChange}
                  error={formik.touched.description && Boolean(formik.errors.description)}
                  helperText={formik.touched.description && formik.errors.description }
                />
            </Grid>
            <Grid item xs={1}>
              <div>
                <Tooltip title="Guardar">
                  <IconButton type="submit">
                    <SaveRounded />
                  </IconButton>
                </Tooltip>
                <Tooltip title="Cancelar">
                  <IconButton onClick={() => setEdit(false)} >
                    <CancelRounded />
                  </IconButton>
                </Tooltip>
              </div>
            </Grid>
          </Grid>
        </form>
      </>
    }
  </Paper>;
};

export default TaskComponent;