import React from "react";
import {
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

import AuthPage from "../../pages/auth";
import CallbackPage from "../../pages/callback";
import HomePage from "../../pages/home";
import TasksPage from "../../pages/tasks";
import PrivateRoute from "./private_route";
import AuthButtonComponent from "./auth_button";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));

/**
 *  All routes for this app
 * 
 */
export const AppRoutesComponent = () => {
    const classes = useStyles();

    return <>
        <div className={classes.root}>
        <AppBar position="static">
            <Toolbar>
            <Typography variant="h6" className={classes.title}>
                Todos App
            </Typography>
            <AuthButtonComponent />
            </Toolbar>
        </AppBar>
        </div>
        <Switch>
            <Route exact path="/auth/" component={AuthPage} />
            <Route path="/auth/callback" component={CallbackPage} />

            <Route exact path="/" component={HomePage} />
            <PrivateRoute exact path="/tasks" component={TasksPage} />
            <Redirect to="/" />
        </Switch>
    </>;
};