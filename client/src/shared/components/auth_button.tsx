import React from 'react';
import { withAuth, WithAuthProps } from '8base-react-sdk';
import { useQuery, useApolloClient } from '@apollo/client';
import { Button } from '@material-ui/core';


import { CURRENT_USER_QUERY } from '../graphql';
import { CurrentUser } from '../responses_types';

/**
 *  Auth Button Utility for logout or login into the app
 * 
 */
const AuthButtonComponent: React.FC<WithAuthProps> = (props) => {
    const { auth: { isAuthorized, authClient, authState } } = props;
    const client = useApolloClient();

    const Logout = () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const { loading, error, data } = useQuery<CurrentUser>(CURRENT_USER_QUERY, {
            context: {
                headers: {
                    authorization: "Bearer " + authState.token
                }
            }
        });

        // Don't show Logout before get user data
        if (loading) {
          return null;
        } else {
            console.log(data);
        }
        return <Button
            color="inherit"
            onClick={async () => {
                await client.clearStore();
                authClient.logout();
            }}
        >
        {data?.user.firstName} Logout
        </Button>
    };

    const Login = () => <Button color="inherit" onClick={() => {
        authClient.authorize();
    }}>Login</Button>;

    return <>{
        isAuthorized ?
            <Logout /> :
            <Login />}
    </>;
};

const AuthButtonComponentWithAuth = withAuth(AuthButtonComponent);

export default AuthButtonComponentWithAuth;
