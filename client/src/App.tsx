import React from 'react';
import { AppProvider } from '8base-react-sdk';
import {
  BrowserRouter as Router,
} from "react-router-dom";
import './App.css';

import AuthClient from './shared/auth';
import { AppRoutesComponent } from './shared/components/app_routes';
import { TasksListResponse } from './shared/responses_types';

const workspaceEndpoint = process.env.REACT_APP_WORKSPACE_ENDPOINT ?? '';
const authProfileID = process.env.REACT_APP_AUTH_PROFILE_ID ?? '';

function App() {
  // We'll print all success messages
  const onRequestSuccess = (request: { [key: string]: any; }) => {
    const { operation } = request;
    const message = operation.getContext()

    if (message) {
      // eslint-disable-next-line no-console
      console.warn(message)
    }
  }

  // We'll print all error messages
  const onRequestError = (request: { [key: string]: any; }) => {
    const { graphQLErrors } = request;
    const hasGraphQLErrors = Array.isArray(graphQLErrors) && graphQLErrors.length > 0

    if (hasGraphQLErrors) {
      graphQLErrors.forEach((error: any) => {
        // eslint-disable-next-line no-console
        console.warn(error.message)
      })
    }
  }

  return (
    <Router>
      <AppProvider
        uri={workspaceEndpoint}
        authClient={AuthClient}
        onRequestError={onRequestError}
        onRequestSuccess={onRequestSuccess}
        introspectionQueryResultData={{}}
        cacheOptions={{
          typePolicies: {
            Query: {
              fields: {
                tasksList: {
                  keyArgs: ["type"],
        
                  // While args.cursor may still be important for requesting
                  // a given page, it no longer has any role to play in the
                  // merge function.
                  merge(existing: TasksListResponse, incoming: TasksListResponse, { readField }) {
                    return incoming;
                  },
                },
              },
            },
          },
        }}
        tablesList={[]}
        applicationsList={[]}
        autoSignUp={false}
        authProfileId={authProfileID}
      >
        <AppRoutesComponent />
      </AppProvider>
    </Router>
  );
}

export default App;
