# 8Base React Test

Before test this app is mandatory to install the 8base-cli and configure your workspace, more info here: [8base Quick Start](https://docs.8base.com/docs/getting-started/quick-start/)

To test the client app follow the next steps:

- Open a terminal in this path and executes: `cd client`
- Next we create a new local env file with this command: `cp .env .env.local`
- Now is mandatory to set all variables in local env file with your 8base workspace configuration
- To finish install all deps with: `yarn install` or `npm install`

To deploy the resolver to 8base follow de next steps:

- Open a terminal in this path and executes: `cd server`
- Now edit the 8base.yml and set your workspaceId
- Install all deps with: `yarn install` or `npm install`
- As final step, run this command in your terminal: `8base deploy`
