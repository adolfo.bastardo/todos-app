/**
 * This file was generated using 8base CLI.
 *
 * To learn more about writing custom GraphQL resolver functions, visit
 * the 8base documentation at:
 *
 * https://docs.8base.com/8base-console/custom-functions/resolvers
 *
 * To update this functions invocation settings, update its configuration block
 * in the projects 8base.yml file:
 *  functions:
 *    setTodoDone:
 *      ...
 *
 * Data that is sent to this function can be accessed on the event argument at:
 *  event.data[KEY_NAME]
 *
 * There are two ways to invoke this function locally:
 *
 *  (1) Explicit file mock file path using '-p' flag:
 *    8base invoke-local setTodoDone -p src/resolvers/setTodoDone/mocks/request.json
 *
 *  (2) Default mock file location using -m flag:
 *    8base invoke-local setTodoDone -m request
 *
 *  Add new mocks to this function to test different input arguments. Mocks can easily be generated
 *  the following generator command:
 *    8base generate mock setTodoDone -m [MOCK_FILE_NAME]
 */

import { FunctionContext, FunctionEvent, FunctionResult } from '8base-cli-types';
import gql from 'graphql-tag';

const TaskSetDone = gql`
  mutation TaskSetDone(
    $id: ID!
    $done: Boolean!,
  ) {
    taskUpdate(
      data: {
        id: $id,
        isDone: $done
      },
      filter: {
        id: $id
      }
    ) {
      id
      name
      description
      isDone
      createdAt
      deletedAt
      updatedAt
    }
  }
`;

type ResolverResult = FunctionResult<{
  success: boolean,
}>;

export default async (
  event: FunctionEvent<{ id: string, done: boolean }>,
  ctx: FunctionContext,
): ResolverResult => {
  try {
    /**
     *  Update task to set as DONE:
     */
    await ctx.api.gqlRequest(TaskSetDone, {
    	id: event.data.id,
    	done: event.data.done
    });

    return {
      data: {
        success: true,
      },
    };

  /* Handle errors for failed GraphQL mutation */
  } catch (e) {
    return {
      data: {
        success: false,
      },
      errors: e
    };
  }
};
